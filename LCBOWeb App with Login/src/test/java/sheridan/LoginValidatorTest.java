package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses123" ) );
	}
	
	@Test
	public void testHasCharactersAndNumbersRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses123" ) );
	}
	
	@Test
	public void testHasCharactersAndNumbersException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName(null) );
	}

	@Test
	public void testHasCharactersAndNumbersBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	@Test
	public void testHasCharactersAndNumbersBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "r1amses" ) );
	}
	
	@Test
	public void testHasEnoughCharactersRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses123" ) );
	}
	
	@Test
	public void testHasEnoughCharactersException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName(null) );
	}

	@Test
	public void testHasEnoughCharactersBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "rams1" ) );
	}
	
	@Test
	public void testHasEnoughCharactersBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "r1amse" ) );
	}
}
