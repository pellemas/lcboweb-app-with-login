package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		
		int characterCount = 0;
		int numberCount = 0;
		
		if(loginName == null)
			return false;
		
		
		for(int i = 0; i < loginName.length(); i++) {
			if(Character.isAlphabetic(loginName.charAt(i))) {
				characterCount++;
			} else if(Character.isDigit(loginName.charAt(i))) {
				numberCount++;
			} else {
				break;
			}
		}
		
		if (characterCount > 0 && numberCount > 0)
			return characterCount + numberCount >= 6;
			
		return false;
	}
}
